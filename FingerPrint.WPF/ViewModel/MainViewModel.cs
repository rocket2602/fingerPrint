using FingerPrint.Base.Binarization;
using FingerPrint.Base.Sceletonization;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Drawing;
using FP = FingerPrint.Base.FingerPrint;
using System.Xml.Linq;
using FingerPrint.WPF.Model;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using FingerPrint.WPF.View;
using FingerPrintScaner.Scaners;
using System.Collections.Generic;
using FingerPrint.Base.Helpers;
using Microsoft.Win32;
using System.Threading.Tasks;
using FingerPrint.Base.Models;

namespace FingerPrint.WPF.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            

            ScanImage = new RelayCommand(ScanImageClick);

            OpenImage = new RelayCommand(OpenImageClick);

            ProccessImage = new RelayCommand(StartWork);

            IdentifyImage = new RelayCommand(FindTemplate);

            ShowDeviceSettingWindow = new RelayCommand(ShowDevicePopup);

            PrevOutput = new RelayCommand(PrevStep);

            NextOutput = new RelayCommand(NextStep);

            SaveTemplate = new RelayCommand(Save);

            Application.Current.MainWindow.Closing += MainWindow_Closing;

        }

        private static string storage = Path.GetFullPath(Path.Combine("..", "..", "..", "_storage", "images"));


        private async void Save()
        {
            if (template == null)
            {
                MessageBox.Show("Click on Proccess btn before", "Save template error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            };

            var model = await HttpHelper.Register(template);

            var fileName = Path.Combine(System.Environment.CurrentDirectory, "Images", model.Id.ToString() + ".jpg");

            _fingerImage.Save(Path.Combine(storage, model.Id.ToString() + ".jpg"));


        }

        private void NextStep()
        {
            i += 1;
            if (i <= 4)
            {
                RefreshImage(_imageResult[i]);
                ProccessDisplay = d[i - 1];
            }
        }

        int i = 4;
        private void PrevStep()
        {
            i -= 1;
            if (i >= 1)
            {
                RefreshImage(_imageResult[i]);
                ProccessDisplay = d[i - 1];

            }
        }

        private string _proccessDisplay = String.Empty;

        public string ProccessDisplay
        {
            get { return _proccessDisplay; }
            private set
            {
                this._proccessDisplay = value; RaisePropertyChanged("ProccessDisplay");
            }
        }

        FutronicScaner _scaner;

        FP _fp;

        private void ScanImageClick()
        {
            //  _fingerImage = null;

            _scaner = new FutronicScaner();

            _scaner.OnTouch += Scaner_OnTouch;

            _scaner.OnException += _scaner_OnException;


            _scaner.Scan();
        }

        private void _scaner_OnException(string message)
        {
            MessageBox.Show(message, "Scanner error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Scaner_OnTouch(bool touch)
        {
            if (touch)
            {
                _fingerImage = _scaner.FingerImage;

                RefreshImage(_fingerImage, true);
            }
        }

        Dispatcher dispatcher = Dispatcher.CurrentDispatcher;


        private void RefreshImage(Image image, bool input = false)
        {
            dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {
                var data = (byte[])new ImageConverter().ConvertTo(image, typeof(byte[]));

                if (input)
                    InputImage = data;
                else
                    OutputImage = data;

            }));

        }

        Dictionary<int, Image> _imageResult;


        List<string> d = new List<string>() { "Binarization", "Sceletonization", "Points", "Classification" };



        public void StartWork()
        {
            if (_fingerImage == null) return;

            IBinarization b = new OtsuAlgorithm();

            ISceletonization s = new ZhangSuenThinning();

            _fp = new FP();

            _imageResult = new Dictionary<int, Image>();

            ProccessDisplay = "Binarization";

            var binImage = b.Binarization(_fingerImage);

            _imageResult.Add(1, binImage);

            RefreshImage(binImage);

            ProccessDisplay = "Sceletonization";

            var skImage = s.Sceletonization(binImage);

            _imageResult.Add(2, skImage);

            RefreshImage(skImage);

            var person = _fp.GetFingerPrint(skImage);

            template = person.Template;

            var xmlTemplate = person.AsXmlTemplate;

            Logger = xmlTemplate.ToString();

            var p = (Image)skImage.Clone();

            DrawSpecialPoints(xmlTemplate, p);

            ProccessDisplay = "Points";

            _imageResult.Add(3, p);

            ProccessDisplay = "Classification";

            var c = (Image)p.Clone();

            _imageResult.Add(4, c);

            DrawSheet();



        }

        private void ShowDevicePopup()
        {
            var deviceVm = new DeviceSettingsWindow();

            deviceVm.ShowDialog();
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }

        byte[] template;

        private async void FindTemplate()
        {
            if (template == null) return;

            var response = await HttpHelper.Identify(template, Row, Column,MinutiaType);

            if (response.Success)
            {
                DisplayResult(response);

               // MessageBox.Show($"Image found {response.Id} {response.Similarity}");
            }
            else
            {
                MessageBox.Show("Something Wrong");
            }

            //var fileName = Path.Combine(System.Environment.CurrentDirectory, "Images", model.Id.ToString() + ".jpg");

            //if (model.IsNew)
            //{
            //    // fingerImage.Save(fileName, ImageFormat.Jpeg);

            //    _storage.Add(new GalleryImagesModel() { Patch = fileName, Name = model.Id.ToString() + ".jpg" });

            //    Logger = $"New finfer {model.Id} added to DB";

            //    return;
            //}


            //Storage.OrderByDescending(o => o.Name == model.Id + ".jpg").ThenBy(o => o.Name);

            //SelectedCustomObject = Storage.Where(x => x.Name == model.Id + ".jpg").First();

            //Logger = $"Finger {model.Id} found";

            //var fp = new FP();

            //float score = fp.Verify(template, fileName);

            // Logger = $"Similarity score between {_inputImagePath} and {fileName} = {score:F3}";



        }

        private void DisplayResult(FingerViewModel response)
        {
            //string root = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            //string[] supportedExtensions = new[] { ".bmp", ".jpeg", ".jpg", ".png", ".tiff" };
            ////   var files = Directory.GetFiles(Path.Combine(root, "Images"), "*.*").Where(s => supportedExtensions.Contains(Path.GetExtension(s).ToLower()));

            //DirectoryInfo folder = new DirectoryInfo(Path.Combine(root, "Images"));
            //FileInfo[] images = folder.GetFiles("*.*");
            //foreach (FileInfo img in images.Where(s => supportedExtensions.Contains(s.Extension.ToLower())))
            //{

            //    _storage.Add(new GalleryImagesModel() { Patch = img.FullName, Name = img.Name });
            //}


            //var resultDialog = new ImageProcessingSettingWindow();

            //resultDialog.ShowDialog();

            var storage = Path.GetFullPath(Path.Combine("..", "..", "..", "_storage", "images"));

            var ids = response.Result.Select(x => x.Key.ToString()).ToArray();

            DirectoryInfo folder = new DirectoryInfo(storage);
            FileInfo[] images = folder.GetFiles("*.*");


            //String[] allfiles = Directory.GetFiles(storage, "*.*", SearchOption.AllDirectories)
                                                    
            //                                         .ToArray();

            foreach (FileInfo img in images.Where(file => ids.Contains(Path.GetFileNameWithoutExtension(file.FullName))))
            {

                _storage.Add(new GalleryImagesModel() { Patch = img.FullName, Name =response.Result.Where(x=>x.Key.ToString().Equals(Path.GetFileNameWithoutExtension(img.FullName))).First().Value.ToString()});
            }

        }

        private Image _fingerImage;

        private Image _proccessImage;



        #region RelayCommand

        public RelayCommand ScanImage { get; private set; }

        public RelayCommand OpenImage { get; private set; }

        public RelayCommand ProccessImage { get; private set; }

        public RelayCommand IdentifyImage { get; private set; }

        public RelayCommand ShowDeviceSettingWindow { get; private set; }

        public RelayCommand PrevOutput { get; private set; }

        public RelayCommand NextOutput { get; private set; }

        public RelayCommand SaveTemplate { get; private set; }

        #endregion
        private void OpenImageClick()
        {
            var open = new OpenFileDialog();

            open.Filter = "Images (*.jpg;*.png;*bmp;*.tif)|*.jpg;*.png;*.bmp;*.tif;";


            if (open.ShowDialog() == true)
            {
                Logger = "Openning image...";

                _fingerImage = Image.FromFile(open.FileName);

                RefreshImage(_fingerImage, true);

            }
        }

        private void DrawSpecialPoints(XElement xmlTemplate, Image img)
        {
            using (var g = Graphics.FromImage(img))
            {
                foreach (var item in xmlTemplate.Elements())
                {
                    var x = Convert.ToInt32(item.Attribute("X").Value);

                    var y = Convert.ToInt32(item.Attribute("Y").Value);

                    // var angle = Math.PI *  Convert.ToInt32(item.Attribute("Direction").Value)/180; 

                    g.DrawEllipse(new Pen(Color.Red, 1), x, y, 5, 5);

                    // Point point2 = new Point((int)( x + Math.Cos(angle) * 3),(int)(y + Math.Sign(angle) * 3));

                    // g.DrawLine(new Pen(Color.Red, 1), new Point(x, y), point2);

                }

                RefreshImage(img);
            }
        }

        private void DrawSheet()
        {

            if (template == null) return;

            var img = (Image)_imageResult[3].Clone();

            var columnWidth = img.Width / _m;

            var rowHeight = img.Height / _n;

            var fp = new FP();

            var sheetPointCount = fp.CallSheet(template, _m, _n, MinutiaType);


            using (var g = Graphics.FromImage(img))
            {

                for (var j = 1; j < _m; j++)
                {
                    g.DrawLine(
                        new Pen(Color.Red, 1f),
                       0, j * rowHeight,
                        img.Width, j * rowHeight);
                }

                for (var i = 1; i < _n; i++)
                {
                    g.DrawLine(
                        new Pen(Color.Red, 1f),
                        i * columnWidth, 0,
                        i * columnWidth, img.Height);
                }



                var columCentr = columnWidth / 2;
                var rowCentr = rowHeight / 2;
                for (var i = 0; i < _m; i++)
                {
                    for (var j = 0; j < _n; j++)
                    {
                        g.DrawString(sheetPointCount[i, j].ToString(), new Font("Arial", 14), new SolidBrush(Color.Red),
                            (float)i * columnWidth + columCentr, (float)j * rowHeight + rowCentr);
                    }
                }
            }

            _imageResult[4] = img;

            RefreshImage(_imageResult[4]);
        }



        private byte[] _inputImage;

        public byte[] InputImage
        {
            get { return _inputImage; }
            set
            {
                _inputImage = value;
                RaisePropertyChanged("InputImage");
            }
        }

        private byte[] _outputImage;

        public byte[] OutputImage
        {
            get { return _outputImage; }
            set
            {
                _outputImage = value;
                RaisePropertyChanged("OutputImage");
            }
        }

        private Int32 _m = 2, _n = 2;

        public int Row
        {
            get { return _m; }
            set { _m = value; DrawSheet(); }
        }

        public int Column
        {
            get { return _n; }
            set { _n = value; DrawSheet(); }
        }


        private string _loggerText;

        public string Logger
        {
            get { return _loggerText; }
            set
            {
                _loggerText += "\n" + value; RaisePropertyChanged("Logger");
            }
        }

        private GalleryImagesModel _selectedItem;

        public static string[] MinutiaList
        {
            get
            {
                return new string[] { "Ending", "Bifurcation", "Other","All" };
            }
        }

        private int _minutiaType = 3;

        public int MinutiaType
        {
            get { return _minutiaType; }
            set { _minutiaType = value; DrawSheet(); }
        }

        public GalleryImagesModel SelectedCustomObject
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem == value)
                {
                    return;
                }

                _selectedItem = value;
                RaisePropertyChanged("SelectedCustomObject");
            }
        }

        private ObservableCollection<GalleryImagesModel> _storage = new ObservableCollection<GalleryImagesModel>();


        public ObservableCollection<GalleryImagesModel> Storage
        {
            get { return _storage; }

        }
    }
}