﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FingerPrint.Base.Models;

namespace FingerPrint.WPF.View
{
    /// <summary>
    /// Interaction logic for ImageProcessingSettingWindow.xaml
    /// </summary>
    public partial class ImageProcessingSettingWindow : Window
    {
        private FingerViewModel response;

        public ImageProcessingSettingWindow()
        {
            InitializeComponent();
        }

        public ImageProcessingSettingWindow(FingerViewModel response):this()
        {
            this.response = response;
        }
    }
}
