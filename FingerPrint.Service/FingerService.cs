﻿using FingerPrint.Models;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FingerPrint.Repository.Repositories;


namespace FingerPrint.Service
{
    public interface IFingerService : IService<FingerTemplate>
    {
        IEnumerable<FingerTemplate> GetAll();
    }

    public class FingerService: Service<FingerTemplate>, IFingerService
    {
        private readonly IRepositoryAsync<FingerTemplate> _repository;


        public FingerService(IRepositoryAsync<FingerTemplate> repository) : base(repository)
        {
            _repository = repository;
        }

        public IEnumerable<FingerTemplate> GetAll()
        {
            // // add business logic here
            return _repository.GetAll();
        }

        public override void Insert(FingerTemplate entity)
        {
            entity.Id = Guid.NewGuid();

            base.Insert(entity);
        }
    }
}
