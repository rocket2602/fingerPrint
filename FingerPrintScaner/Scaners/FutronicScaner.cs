﻿using ScanAPIHelper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FingerPrintScaner.Scaners
{
    public class FutronicScaner : FingerScanerBase
    {
        #region const
        const int FTR_ERROR_EMPTY_FRAME = 4306; /* ERROR_EMPTY */
        const int FTR_ERROR_MOVABLE_FINGER = 0x20000001;
        const int FTR_ERROR_NO_FRAME = 0x20000002;
        const int FTR_ERROR_USER_CANCELED = 0x20000003;
        const int FTR_ERROR_HARDWARE_INCOMPATIBLE = 0x20000004;
        const int FTR_ERROR_FIRMWARE_INCOMPATIBLE = 0x20000005;
        const int FTR_ERROR_INVALID_AUTHORIZATION_CODE = 0x20000006;

        /* Other return codes are Windows-compatible */
        const int ERROR_NO_MORE_ITEMS = 259;                // ERROR_NO_MORE_ITEMS
        const int ERROR_NOT_ENOUGH_MEMORY = 8;              // ERROR_NOT_ENOUGH_MEMORY
        const int ERROR_NO_SYSTEM_RESOURCES = 1450;         // ERROR_NO_SYSTEM_RESOURCES
        const int ERROR_TIMEOUT = 1460;                     // ERROR_TIMEOUT
        const int ERROR_NOT_READY = 21;                     // ERROR_NOT_READY
        const int ERROR_BAD_CONFIGURATION = 1610;           // ERROR_BAD_CONFIGURATION
        const int ERROR_INVALID_PARAMETER = 87;             // ERROR_INVALID_PARAMETER
        const int ERROR_CALL_NOT_IMPLEMENTED = 120;         // ERROR_CALL_NOT_IMPLEMENTED
        const int ERROR_NOT_SUPPORTED = 50;                 // ERROR_NOT_SUPPORTED
        const int ERROR_WRITE_PROTECT = 19;                 // ERROR_WRITE_PROTECT
        const int ERROR_MESSAGE_EXCEEDS_MAX_SIZE = 4336;    // ERROR_MESSAGE_EXCEEDS_MAX_SIZE
        const int ERROR_DEVICE_CONNECTED = 1;
        #endregion

        #region fields

        private Device _device;

        private bool is_cancaletion = false;

        private byte _ScanMode = 0;

        private byte[] _Frame;

        private bool isTouche;

        #endregion

        public override event Action<bool> OnTouch;

        public override event Action<string> OnException;

        public override void Connect()
        {
            int defaultInterface = ScanAPIHelper.Device.BaseInterface;

            FTRSCAN_INTERFACE_STATUS[] status = ScanAPIHelper.Device.GetInterfaces();

            for (int interfaceNumber = 0; interfaceNumber < status.Length; interfaceNumber++)
            {
                if (status[interfaceNumber] == FTRSCAN_INTERFACE_STATUS.FTRSCAN_INTERFACE_STATUS_CONNECTED)
                {
                    return;
                }
            }

            throw new ScanAPIException(ERROR_DEVICE_CONNECTED, $"Error connent to Futronic scaner. Please, make sure that device is plugged.");
        }

        public override void Disconnect()
        {
            if (_device != null)
            {
                _device.Dispose();
                _device = null;
            }
        }

        public override void Scan()
        {
            try
            {
                Connect();

                _device = new Device();

                _device.Open();

                _device.InvertImage = true;

                var WorkerThread = new Thread(new ThreadStart(GetImage));

                WorkerThread.Start();
            }

            catch (ScanAPIException ex)
            {
                Disconnect();

                OnException(ex.Message);
            }
        }

        public override string Name
        {
            get
            {
                return Name;
            }

            set
            {
                Name = "Futronic";
            }
        }

        public void GetDeviceInfo()
        {
            //VersionInfo version = m_hDevice.VersionInformation;
            //var v = version.APIVersion.ToString();
            //var hadwareV = version.HardwareVersion.ToString();
            //var FimvareV = version.FirmwareVersion.ToString();

            //var m_chkDetectFakeFinger = m_hDevice.DetectFakeFinger;

            //var m_bIsLFDSupported = false;

            //string m_lblCompatibility = String.Empty;

            //DeviceInfo dinfo = m_hDevice.Information;
            //switch (dinfo.DeviceCompatibility)
            //{
            //    case 0:
            //    case 1:
            //    case 4:
            //    case 11:
            //        m_lblCompatibility = "FS80";
            //        m_bIsLFDSupported = true;
            //        break;
            //    case 5:
            //        m_lblCompatibility = "FS88";
            //        m_bIsLFDSupported = true;
            //        break;
            //    case 7:
            //        m_lblCompatibility = "FS50";
            //        break;
            //    case 8:
            //        m_lblCompatibility = "FS60";
            //        break;
            //    case 9:
            //        m_lblCompatibility = "FS25";
            //        m_bIsLFDSupported = true;
            //        break;
            //    case 10:
            //        m_lblCompatibility = "FS10";
            //        break;
            //    case 13:
            //        m_lblCompatibility = "FS80H";
            //        m_bIsLFDSupported = true;
            //        break;
            //    case 14:
            //        m_lblCompatibility = "FS88H";
            //        m_bIsLFDSupported = true;
            //        break;
            //    case 15:
            //        m_lblCompatibility = "FS64";
            //        break;
            //    case 16:
            //        m_lblCompatibility = "FS26E";
            //        break;
            //    case 17:
            //        m_lblCompatibility = "FS80HS";
            //        break;
            //    case 18:
            //        m_lblCompatibility = "FS26";
            //        break;
            //    default:
            //        m_lblCompatibility = "Unknown device";
            //        break;
            //}

            //var m_lblCurrentImageSize = m_hDevice.ImageSize.ToString();


            //m_chkDetectFakeFinger = m_bIsLFDSupported;
        }

        private void GetImage()
        {
            while (!is_cancaletion)
            {
                GetFrame();

                if (_Frame != null)
                {
                    var myFile = new MyBitmapFile(_device.ImageSize.Width, _device.ImageSize.Height, _Frame);

                    var BmpStream = new MemoryStream(myFile.BitmatFileData);

                    FingerImage = new Bitmap(BmpStream);

                    OnTouch(true);
                }

                Thread.Sleep(10);
            }

            OnTouch(false);

        }

        private void GetFrame()
        {
            try
            {
                if (_ScanMode == 0)
                    _Frame = _device.GetFrame();
                else
                    _Frame = _device.GetImage(_ScanMode);

                this.isTouche = true;

            }
            catch (ScanAPIException ex)
            {
                if (isTouche)
                {
                    is_cancaletion = true;
                }
                //  ShowError(ex);
            }
        }
    }
}
