﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrintScaner
{
    public abstract class FingerScanerBase
    {

        public  Image FingerImage { get; protected set; }

        public abstract void Connect();

        public abstract void Disconnect();

        public abstract void Scan();

        public virtual event Action<bool> OnTouch;

        public virtual event Action<string> OnException;

        public abstract string Name { get; set; }

    }
}
