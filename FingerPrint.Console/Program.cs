﻿using FingerPrint.Base.Binarization;
using System.Drawing;
using fp = FingerPrint.Base.FingerPrint;
using FingerPrint.Base.Sceletonization;
using System;
using System.Linq;
using System.IO;
using FingerPrint.Base.Helpers;
using System.Threading.Tasks;

namespace FingerPrint.Console
{
    class Program
    {


        static void Main(string[] args)
        {

            //CreateTestDBTemplates();



            var line = File.ReadAllLines("binarization.txt");

            System.Console.WriteLine(line.Select(x=>Convert.ToDouble(x)).Average());



            System.Console.ReadKey();
        }


        static object lockObject = new object(); 

        public static async void CreateTestDBTemplates()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();


            var RootFolder = Path.GetFullPath(Path.Combine("..", "..", "..", "_resource", "TestDatabase"));


            var storage = Path.GetFullPath(Path.Combine("..", "..", "..", "_storage", "images"));

            var supportedExtensions = new string[] { ".jpg", ".png", ".bmp", ".jpe", ".jpeg", ".ico", ".eps", ".tif", ".tiff" };

            String[] allfiles = Directory.GetFiles(RootFolder, "*.*", SearchOption.AllDirectories)
                                                     .Where(file => supportedExtensions.Any(x => file.EndsWith(x, StringComparison.OrdinalIgnoreCase)))
                                                     .ToArray();

            var count = allfiles.Length;

            System.Console.WriteLine($"Found {count} images in directory");

            IBinarization b = new OtsuAlgorithm();

            ISceletonization s = new ZhangSuenThinning();

           

            int i = 0;

            var fp = new fp();

            foreach (var item in allfiles.Take(100))
            {
                //  Parallel.ForEach(allfiles.Take(50), (item) => {

                System.Console.WriteLine($"Process {i++} of {--count}");
                System.Console.WriteLine(item);
                try
                {

                    System.Console.ResetColor();
                    var image = Image.FromFile(item);

                    var template = fp.GetBinaryTemplate(b, s, image);


                    //  HttpHelper.Register(template).ConfigureAwait(false);

                    //lock (lockObject)
                    //{

                    //if (response.Success)
                    //                            image.Save(Path.Combine(storage, response.Id.ToString() + Path.GetExtension(item)));
                    //else
                    //    System.Console.WriteLine(response.ErrorMessage);
                    //  }

                }
                catch (Exception ex)
                    {
                        System.Console.BackgroundColor = ConsoleColor.Red;
                        System.Console.WriteLine(ex.Message);
                    }
                }
             //   });

            



            watch.Stop();


            System.Console.WriteLine("Eecution Time: {0} sec.", TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds).TotalSeconds);

        }


    }
}
