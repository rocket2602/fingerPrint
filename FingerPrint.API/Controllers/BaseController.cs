﻿using Ninject;
using Repository.Pattern.UnitOfWork;
using System.Web.Http;

namespace FingerPrint.API.Controllers
{
    public abstract class BaseController : ApiController
    {
        [Inject]
        public IUnitOfWorkAsync _unitOfWorkAsync { get; set; }


    }
}