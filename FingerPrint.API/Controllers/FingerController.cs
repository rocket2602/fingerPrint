﻿using FingerPrint.Models;
using FingerPrint.Service;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using fp = FingerPrint.Base.FingerPrint;
using FingerPrint.API.Models;

namespace FingerPrint.API.Controllers
{
    [RoutePrefix("finger")]
    public class FingerController : BaseController
    {
        private readonly IFingerService _fingerService;

        public FingerController(IFingerService fingerService)
        {
            _fingerService = fingerService;
        }

        public sealed class SomePostRequest
        {
            public Guid Id { get; set; }
            public byte[] Content { get; set; }

            public int Row { get; set; }

            public int Column { get; set; }

            public int Minutia { get; set; }
        }


[Route("template/identify")]
public FingerResponce Identify([FromBody]SomePostRequest data)
{
    try
    {
        var fingersDB = _fingerService.GetAll();

        var result = new fp().Classification(data.Content, fingersDB, data.Row, data.Column,data.Minutia);

        return new FingerResponce() { Success = true, Result = result };

    }
    catch (Exception ex)
    {
        return new FingerResponce() { Success = false, ErrorMessage = ex.Message };
    }
}

        [Route("template/set")]
        public async Task<FingerResponce> RegisterNewTemplate([FromBody]SomePostRequest data)
        {
            try
            {
                var f = new FingerTemplate()
                {
                    Template = data.Content,
                    CreatedOn = DateTime.Now,
                };

                _fingerService.Insert(f);

                await _unitOfWorkAsync.SaveChangesAsync();


                return new FingerResponce() { Success = true, Id = f.Id };

            }
            catch (Exception ex)
            {
                return new FingerResponce() { Success = false, ErrorMessage = ex.Message };
            }

        }

    }
}