﻿using FingerPrint.Models;
using FingerPrint.Service;
using Ninject;
using Repository.Pattern.DataContext;
using Repository.Pattern.Ef6;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FingerPrint.API.App_Start
{
    public class IoSConfig
    {
        public class NinjectDependencyResolver : IDependencyResolver
        {
            private IKernel kernel;
            public NinjectDependencyResolver(IKernel kernelParam)
            {
                kernel = kernelParam;
                AddBindings();
            }
            public object GetService(Type serviceType)
            {
                return kernel.TryGet(serviceType);
            }
            public IEnumerable<object> GetServices(Type serviceType)
            {
                return kernel.GetAll(serviceType);
            }
            private void AddBindings()
            {
                kernel.Bind<IDataContextAsync>().To<FingerPrintEntities>().InSingletonScope();
                kernel.Bind<IUnitOfWorkAsync>().To<UnitOfWork>().InSingletonScope();

                kernel.Bind<IFingerService>().To<FingerService>();
                kernel.Bind<IRepositoryAsync<FingerTemplate>>().To<Repository<FingerTemplate>>();
            }
        }
    }
}