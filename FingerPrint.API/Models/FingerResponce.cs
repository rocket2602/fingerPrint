﻿using System;
using System.Collections.Generic;

namespace FingerPrint.API.Models
{
    public class FingerResponce
    {
        public Guid Id { get; set; }

        public double Similarity { get; set; }

        public bool IsNew { get; set; }

        public string ErrorMessage { get; set; }

        public bool  Success { get; set; }

        public IEnumerable<KeyValuePair<Guid, double>> Result { get; set; }


    }
}