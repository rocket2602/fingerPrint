﻿using FingerPrint.Models;
using Repository.Pattern.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace FingerPrint.Repository.Repositories
{
    public static class FingerRepository
    {
        public static IEnumerable<FingerTemplate> GetAll(this IRepository<FingerTemplate> repository)
        {
            return repository.Queryable().AsEnumerable();
               
        }
    }
}
