﻿using FingerPrint.Base.Binarization;
using FingerPrint.Base.Common;
using FingerPrint.Base.Sceletonization;
using System;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;
using SourceAFIS.Simple;
using aa = FingerPrint.Models;
using System.Xml.Linq;
using SourceAFIS.Templates;
using System.Threading.Tasks;
using System.IO;

namespace FingerPrint.Base
{
    public class FingerPrint
    {


        [Serializable]
        class MyFingerprint : Fingerprint
        {
            public string Filename;
        }

        // Inherit from Person in order to add Name field
        [Serializable]
        class MyPerson : Person
        {
            public Guid FingerId;
        }

        public FingerPrint()
        {

        }

        object lockObj = new object();

        public Person GetTemplate(IBinarization binarization, ISceletonization sceletonization, Image image)
        {



            var binarImage = binarization.Binarization(image);




            // binarImage.Save("binarization.jpg", ImageFormat.Jpeg);

            var sceleton = sceletonization.Sceletonization(binarImage);




            // sceleton.Save("sceletonization.jpg", ImageFormat.Jpeg);

            var fp = new MyFingerprint();


            var converter = new ImageConverter();

            byte[] imgArray = (byte[])converter.ConvertTo(sceleton, typeof(byte[]));

            fp.AsBitmapSource = ImageDataConverter.ImageFromBuffer(imgArray);

            var person = new MyPerson();

            // Add fingerprint to the person
            person.Fingerprints.Add(fp);

            // Execute extraction in order to initialize fp.Template
            Console.WriteLine(" Extracting template...");

            var Afis = new AfisEngine();
            var watch = System.Diagnostics.Stopwatch.StartNew();

            Afis.Extract(person);

            watch.Stop();

            System.Console.WriteLine("Eecution Time of Binarization: {0} sec.", TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds).TotalSeconds);


            File.AppendAllText("binarization.txt", $"{TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds).TotalSeconds}{Environment.NewLine}");



            return person;

        }

        public byte[] GetBinaryTemplate(IBinarization binarization, ISceletonization sceletonization, Image image)
        {

            var person = GetTemplate(binarization, sceletonization, image);

            return person.Fingerprints.First().Template;

        }


        public IEnumerable<KeyValuePair<Guid, double>> Classification(byte[] template, IEnumerable<aa.FingerTemplate> fingersDBTrmplates, int row, int column, int minutiaType)
        {
            var parent = CallSheet(template, row, column, minutiaType);

            var dictionary = new Dictionary<Guid, double>();

            Parallel.ForEach(fingersDBTrmplates, (item) =>
            {
                var sheet = CallSheet(item.Template, row, column, minutiaType);

                dictionary.Add(item.Id, DifferentComputed((int[,])parent.Clone(), sheet));
            });
            

            var foundElement = dictionary.OrderBy(pair => pair.Value).Take(10);

            return foundElement;
        }


        public Guid Identify(byte[] content, IEnumerable<aa.FingerTemplate> fingersDB, int row, int column)
        {
            var probe = new MyPerson();

            var fp = new MyFingerprint();

            fp.Template = content;

            probe.Fingerprints.Add(fp);

            var candidates = new List<MyPerson>();

            foreach (var item in fingersDB)
            {
                var p = new MyPerson() { FingerId = item.Id };

                p.Fingerprints.Add(new MyFingerprint() { Template = item.Template });

                candidates.Add(p);
            }




            var Afis = new AfisEngine();

            Afis.Threshold = 10;

            MyPerson match = Afis.Identify(probe, candidates).FirstOrDefault() as MyPerson;

            if (match == null)
            {
                return Guid.Empty;
            }

            return match.FingerId;

        }



        public float Verify(byte[] template, string fileName)
        {
            var person = new Person();

            person.Fingerprints.Add(new Fingerprint() { Template = template });

            var Afis = new AfisEngine();

            return Afis.Verify(person, GetTemplate(new OtsuAlgorithm(), new ZhangSuenThinning(), Image.FromFile(fileName)));
        }

        public SourceAFIS.Simple.Fingerprint GetFingerPrint(Image image)
        {
            var fp = new MyFingerprint();

            var converter = new ImageConverter();

            byte[] imgArray = (byte[])new ImageConverter().ConvertTo(image, typeof(byte[]));

            fp.AsBitmapSource = ImageDataConverter.ImageFromBuffer(imgArray);

            var person = new MyPerson();

            // Add fingerprint to the person
            person.Fingerprints.Add(fp);

            var Afis = new AfisEngine();

            Afis.Extract(person);

            return person.Fingerprints.First();
        }


        public int[,] CallSheet(byte[] template, int row, int column, int minutiaType)
        {
            var format = new CompactFormat();

            var templateBld = format.Import(template);



            return ComputedPiintsInSheet(row, column, templateBld, minutiaType);

        }

        public int[,] CallSheet(XElement template, int row, int column, int minutiaType)
        {
            var format = new XmlFormat();

            var templateBld = format.Import(template);

            return ComputedPiintsInSheet(row, column, templateBld, minutiaType);

        }

        private static int[,] ComputedPiintsInSheet(int row, int column, TemplateBuilder templateBld, int minutiaType)
        {



            var sheetPointCount = new int[row, column];

            var rowHeight = templateBld.OriginalHeight / column;

            var columnWidth = templateBld.OriginalWidth / row;


            for (var i = 0; i < row; i++)
                for (var j = 0; j < column; j++)
                {
                    var points = templateBld.Minutiae.Where(point =>
                    point.Position.X >= i * columnWidth && point.Position.X <= i * columnWidth + columnWidth
                    && point.Position.Y >= j * rowHeight && point.Position.Y <= j * rowHeight + rowHeight);

                    if (minutiaType != 3)//magis int 
                        points = points.Where(x => x.Type == (TemplateBuilder.MinutiaType)minutiaType);




                    sheetPointCount[i, j] = points.Count();
                }

            return sheetPointCount;
        }

        public double DifferentComputed(int[,] array1, int[,] array2)
        {
            int width = array1.GetLength(0);
            int height = array2.GetLength(1);

            double result = 0;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    var a1 = array1[i, j];
                    var a2 = array2[i, j];

                    var temp = Math.Pow(Math.Abs(a1 - a2), 2);
                    result += temp;
                }
            }

            var res = result / (width * height);

            return res;

        }
    }
}
