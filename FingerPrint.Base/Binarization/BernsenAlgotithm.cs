﻿using FingerPrint.Base.Binarization.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Binarization
{
    public class BernsenAlgotithm : IBinarization
    {
        private Image _image;

        private int _imageSize;

        private int windowSize;

        private int eps;

        public int threshold
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public BernsenAlgotithm(int windowSize, int eps)
        {
            this.eps = eps;
            this.windowSize = windowSize;

        }

        public  Image Binarization(Image image)
        {
            this._image = image;
            _imageSize = image.Width * image.Height;
          
            var myBitmap = new Bitmap(_image.Width, _image.Height);

            try
            {
                if (windowSize % 2 != 0)
                {
                    //odczyt obrazu
                    using (Bitmap bmp = new Bitmap(_image))
                    {
                        Color originalColor;
                        Color framePixelColor;
                        GrayPixel[] _pixels = new GrayPixel[_imageSize];
                        GrayPixel _framePixelTemp = new GrayPixel();
                        GrayPixel[] _framePixels = new GrayPixel[(windowSize - 1) * (windowSize - 1)];

                        int j = 0;
                        int counter = 0;
                        int distance = Convert.ToInt32((windowSize - 1) * 0.5);
                        int _maxTempValue = 0;
                        int _minTempValue = 0;
                        for (int i = 0; i < _imageSize; i++)
                        {
                            _pixels[i] = new GrayPixel();
                        }

                        for (int i = 0; i < (windowSize - 1) * (windowSize - 1); i++)
                        {
                            _framePixels[i] = new GrayPixel();
                        }

                        #region
                        for (int x = 0; x < _image.Width; x++)
                        {
                            for (int y = 0; y < _image.Height; y++)
                            {
                                originalColor = bmp.GetPixel(x, y);
                                //create the grayscale version of the pixel
                                _pixels[j].value = (int)((originalColor.R * .3) + (originalColor.G * .59) + (originalColor.B * .11));
                                _pixels[j].R = originalColor.R;
                                _pixels[j].G = originalColor.G;
                                _pixels[j].B = originalColor.B;
                                _pixels[j].x = x;
                                _pixels[j].y = y;

                                //tworzymy liste o wielkosci okna naokolo pixela 
                                if (x >= distance && x < _image.Width - distance)
                                {
                                    if (y >= distance && y < _image.Height - distance)
                                    {
                                        ;
                                        //_framePixels.Clear();
                                        counter = 0;
                                        //i,k - obramowanie okna po ktorym sie poruszam naokolo danego pixela
                                        //pobieram pixele naokolo pixela i licze prog za pomoca ich wartosci
                                        for (int i = x - distance; i < x + distance; i++)
                                        {
                                            for (int k = y - distance; k < y + distance; k++)
                                            {
                                                ;
                                                //pobieram wartosci dla kazdego pixela z okna i odrazu licze prog lokalny dla obecnego pixela(centralnego z okna)
                                                framePixelColor = bmp.GetPixel(i, k);
                                                _framePixels[counter].value = (int)((framePixelColor.R * .3) + (framePixelColor.G * .59) + (framePixelColor.B * .11));
                                                _framePixels[counter].x = i;
                                                _framePixels[counter].y = k;
                                                //lista pixeli w oknie - zle dodaje do listy? WTF - do poprawy
                                                //_framePixels.Add(_framePixelTemp);
                                                counter++;
                                            }
                                        }
                                        //obliczenia na kazdym pixelu w liscie w danej chwili by obliczyc prog dla pixela na pozycji globalnej x,y

                                        _maxTempValue = _framePixels.Max(max => max.value); //max

                                        // min
                                        _minTempValue = _framePixels.Min(min => min.value);
                                        // oblicz prog dla danego globalnego pixela
                                        if (_maxTempValue - _minTempValue <= eps)
                                        {
                                            _pixels[j].threshold = 121;//Convert.ToInt32((_maxTempValue + _minTempValue) * 0.5);
                                        }
                                        else
                                        {
                                            _pixels[j].threshold = Convert.ToInt32((_maxTempValue + _minTempValue) * 0.5);
                                        }
                                    }
                                }
                                else
                                {
                                    //tu trafiaja przypadki, kiedy pixel jest za blisko brzegu obrazka - nie ruszac pixeli i wziac je z oryginalnego obrazka

                                }


                                j++;
                            }
                        }
                        #endregion
                        j = 0;
                        for (int Xcount = 0; Xcount < myBitmap.Width; Xcount++)
                        {
                            for (int Ycount = 0; Ycount < myBitmap.Height; Ycount++)
                            {
                                if (Xcount >= windowSize && Ycount >= windowSize && Xcount <= myBitmap.Width - windowSize && Ycount <= myBitmap.Height - windowSize)
                                {
                                    if (_pixels[j].value > _pixels[j].threshold)
                                    {
                                        myBitmap.SetPixel(Xcount, Ycount, Color.White);
                                    }
                                    else if (_pixels[j].value <= _pixels[j].threshold)
                                    {
                                        myBitmap.SetPixel(Xcount, Ycount, Color.Black);
                                    }
                                }
                                else
                                {
                                    myBitmap.SetPixel(Xcount, Ycount, Color.FromArgb(_pixels[j].R, _pixels[j].G, _pixels[j].B));
                                }
                                j++;

                            }
                        }

                        _image = myBitmap;
                    }
                }
                else { return _image; }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _image;

        }
    }
}
