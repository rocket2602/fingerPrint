﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Binarization.Models
{
    public class GrayPixel
    {
        public int value { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
        public int threshold { get; set; } //prog lokalny dla danego pixela
    }
}
