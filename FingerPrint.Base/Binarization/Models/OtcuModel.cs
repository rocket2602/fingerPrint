﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Binarization.Models
{
    public class Probability
    {
        public int pixel { get; set; }
        public double probability { get; set; }
    }

    public class Variance
    {
        public int threshold { get; set; }
        public double interViariance { get; set; }
    }
}
