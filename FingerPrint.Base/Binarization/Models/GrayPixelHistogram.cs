﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Binarization.Models
{
    public class GrayPixelHistogram
    {
        public int Value { get; set; } //from 0 to 255
        public int Count { get; set; }
    }
}
