﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Binarization
{
    public interface IBinarization
    {
        Image Binarization(Image image);

        int threshold { get; set; }

    }
}
