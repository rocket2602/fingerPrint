﻿using FingerPrint.Base.Binarization.Models;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Binarization
{
    public class OtsuAlgorithm : IBinarization
    {

        private Image _image;

        private int _imageSize;

        private int _maxPixelValue = 256;

        private GrayPixel[] _originalPixels;

        public int _threshold { get; set; }

        public int threshold
        {
            get
            {
                return _threshold;
            }

            set
            {
                _threshold = value;
            }
        }

        public Image Binarization(Image image)
        {
            this._image = image;

            this._imageSize = this._image.Height * this._image.Width;

            this._originalPixels = new GrayPixel[_imageSize];

            var histogram = GetGrayHistogramFromImage(_image);
            //def
            var _pixelProbability = new Probability[_maxPixelValue];

            var _thresholdVariance = new Variance[_maxPixelValue];
            var threshold = new Variance();
            var myBitmap = new Bitmap(_image.Width, _image.Height);

            for (int i = 0; i < _maxPixelValue; i++)
            {
                _pixelProbability[i] = new Probability();
                _thresholdVariance[i] = new Variance();
            }

            int _counter = 0;
            double _objectProbability = 0;
            double _groundProbability = 0;
            double _objectAverage = 0;
            double _groundAverage = 0;
            //int _threshold = 0;

            //calculate probability for each pixel and store data
            foreach (var item in histogram)
            {
                _pixelProbability[_counter].pixel = item.Value;
                _pixelProbability[_counter].probability = ((double)item.Count / (double)_imageSize);
                //Console.WriteLine((float)item.Count / (float)_imageSize);
                _counter++;
            }

            //for each threshold -> t
            for (int t = 0; t < _maxPixelValue; t++)
            {
                _objectProbability = 0;
                _groundProbability = 0;
                _objectAverage = 0;
                _groundAverage = 0;

                //calculate probability of object
                for (int i = 0; i <= t; i++)
                {
                    _objectProbability += _pixelProbability[i].probability;
                }

                //calculate probability of ground
                for (int i = t + 1; i < _maxPixelValue; i++)
                {
                    _groundProbability += _pixelProbability[i].probability;
                }

                //calculate average of object
                for (int i = 0; i <= t; i++)
                {
                    _objectAverage += (i * _pixelProbability[i].probability / _objectProbability);
                }

                //calculate average of ground
                for (int i = t + 1; i < _maxPixelValue; i++)
                {
                    _groundAverage += (i * _pixelProbability[i].probability / _groundProbability);
                }

                //calculate viariance for each threshold -> t
                _thresholdVariance[t].threshold = t;
                _thresholdVariance[t].interViariance = _objectProbability * _groundProbability * (_objectAverage - _groundAverage) * (_objectAverage - _groundAverage);
            }
            //_threshold = _thresholdVariance.GroupBy(x => x.interViariance).Select(group => group.Where(x => x.interViariance == group.Max(y => y.interViariance))).First();
            threshold = _thresholdVariance.MaxBy(x => x.interViariance);
            //GroupBy(x => x.Title).Select(group => group.Where(x => x.Price == group.Max(y => y.Price)).First());


            int j = 0;
            for (int Xcount = 0; Xcount < myBitmap.Width; Xcount++)
            {
                for (int Ycount = 0; Ycount < myBitmap.Height; Ycount++)
                {
                    if (_originalPixels[j].value > threshold.threshold)
                    {
                        myBitmap.SetPixel(Xcount, Ycount, Color.White);
                    }
                    else if (_originalPixels[j].value < threshold.threshold)
                    {
                        myBitmap.SetPixel(Xcount, Ycount, Color.Black);
                    }

                    j++;
                }

            }

            _threshold = threshold.threshold;//out

            return myBitmap;
        }



        private GrayPixelHistogram[] GetGrayHistogramFromImage(Image image)
        {
            int j = 0;

            using (Bitmap bmp = new Bitmap(image))
            {
                Color originalColor;
                GrayPixelHistogram[] pixels = new GrayPixelHistogram[_imageSize];
                GrayPixelHistogram[] allHstValues = new GrayPixelHistogram[_maxPixelValue];
                GrayPixelHistogram[] sortedPixels = new GrayPixelHistogram[_maxPixelValue];

                for (int i = 0; i < _imageSize; i++)
                {
                    pixels[i] = new GrayPixelHistogram();
                    _originalPixels[i] = new GrayPixel();
                }

                for (int i = 0; i < _maxPixelValue; i++)
                {
                    allHstValues[i] = new GrayPixelHistogram();
                    sortedPixels[i] = new GrayPixelHistogram();
                }
                for (int x = 0; x < image.Width; x++)
                {
                    for (int y = 0; y < image.Height; y++)
                    {
                        originalColor = bmp.GetPixel(x, y);
                        //create the grayscale version of the pixel
                        pixels[j].Value = (int)((originalColor.R * .3) + (originalColor.G * .59) + (originalColor.B * .11));
                        _originalPixels[j].value = pixels[j].Value;
                        _originalPixels[j].R = originalColor.R;
                        _originalPixels[j].G = originalColor.G;
                        _originalPixels[j].B = originalColor.B;
                        _originalPixels[j].x = x;
                        _originalPixels[j].y = y;

                        j++;
                    }
                }

                var listOfUniqueVectors = pixels.GroupBy(l => l.Value).Select(g => new
                {
                    Value = g.Key,
                    Count = g.Select(l => l.Value).Count()
                });

                var sortedList = listOfUniqueVectors.OrderBy(r => r.Value);

                allHstValues = GetAllPossibleConfigurationsGrayPixels();

                //int _counter = 0;
                //foreach (var item in sortedList)
                //{
                //    sortedPixels[_counter].Value = item.Value;
                //    sortedPixels[_counter].Count = item.Count;
                //    _counter++;
                //}

                //rewrite vectors into empty matrix
                for (int i = 0; i < _maxPixelValue; i++)
                {
                    foreach (var item in sortedList)
                    {
                        if (item.Value == allHstValues[i].Value)
                        {
                            allHstValues[i].Value = Convert.ToInt32(item.Value);
                            allHstValues[i].Count = item.Count;
                        }
                    }
                }
                return allHstValues;
            }

        }


        private GrayPixelHistogram[] GetAllPossibleConfigurationsGrayPixels()
        {
            var hst = new GrayPixelHistogram[_maxPixelValue];

            for (int i = 0; i < _maxPixelValue; i++)
                hst[i] = new GrayPixelHistogram();

            for (int i = 0; i < _maxPixelValue; i++)
            {
                hst[i].Value = i;
                hst[i].Count = 0;
            }

            return hst;
        }

    }
}
