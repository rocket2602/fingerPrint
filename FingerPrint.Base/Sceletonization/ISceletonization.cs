﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Sceletonization
{
    public interface ISceletonization
    {
        Image Sceletonization(Image image);
    }
}
