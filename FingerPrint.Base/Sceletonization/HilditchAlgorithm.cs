﻿using FingerPrint.Base.Common;
using FingerPrint.Base.Sceletonization;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Sceletonization
{
    public class HilditchAlgorithm : ISceletonization
    {

        public int[,] ThinnerHilditch(int[,] input)
        {
            int lWidth = input.GetLength(0);
            int lHeight = input.GetLength(1);

            bool IsModified = true;
            int Counter = 1;
            int[] nnb = new int[9];
            //去掉边框像素
            for (int i = 0; i < lWidth; i++)
            {
                input[i, 0] = 0;
                input[i, lHeight - 1] = 0;
            }
            for (int j = 0; j < lHeight; j++)
            {
                input[0, j] = 0;
                input[lWidth - 1, j] = 0;
            }
            do
            {
                Counter++;
                IsModified = false;
                int[,] nb = new int[3, 3];
                for (int i = 1; i < lWidth; i++)
                {
                    for (int j = 1; j < lHeight; j++)
                    {
                        //条件1必须为黑点
                        if (input[i, j] != 1)
                        {
                            continue;
                        }

                        //取3*3领域
                        for (int m = 0; m < 3; m++)
                        {
                            for (int n = 0; n < 3; n++)
                            {
                                nb[m, n] = input[i - 1 + m, j - 1 + n];

                            }

                        }
                        //复制
                        nnb[0] = nb[2, 1] == 1 ? 0 : 1;
                        nnb[1] = nb[2, 0] == 1 ? 0 : 1;
                        nnb[2] = nb[1, 0] == 1 ? 0 : 1;
                        nnb[3] = nb[0, 0] == 1 ? 0 : 1;
                        nnb[4] = nb[0, 1] == 1 ? 0 : 1;
                        nnb[5] = nb[0, 2] == 1 ? 0 : 1;
                        nnb[6] = nb[1, 2] == 1 ? 0 : 1;
                        nnb[7] = nb[2, 2] == 1 ? 0 : 1;

                        // 条件2：p0,p2,p4,p6 不皆为前景点 
                        if (nnb[0] == 0 && nnb[2] == 0 && nnb[4] == 0 && nnb[6] == 0)
                        {
                            continue;
                        }
                        // 条件3: p0~p7至少两个是前景点 
                        int iCount = 0;
                        for (int ii = 0; ii < 8; ii++)
                        {
                            iCount += nnb[ii];
                        }
                        if (iCount > 6) continue;

                        // 条件4：联结数等于1 
                        if (DetectConnectivity(nnb) != 1)
                        {
                            continue;
                        }
                        // 条件5: 假设p2已标记删除，则令p2为背景，不改变p的联结数 
                        if (input[i, j - 1] == -1)
                        {
                            nnb[2] = 1;
                            if (DetectConnectivity(nnb) != 1)
                                continue;
                            nnb[2] = 0;
                        }
                        // 条件6: 假设p4已标记删除，则令p4为背景，不改变p的联结数 
                        if (input[i, j + 1] == -1)
                        {
                            nnb[6] = 1;
                            if (DetectConnectivity(nnb) != 1)
                                continue;
                            nnb[6] = 0;
                        }

                        input[i, j] = -1;
                        IsModified = true;
                    }
                }
                for (int i = 0; i < lWidth; i++)
                {
                    for (int j = 0; j < lHeight; j++)
                    {
                        if (input[i, j] == -1)
                        {
                            input[i, j] = 0;
                        }
                    }
                }

            } while (IsModified);

            return input;
        }

        private int DetectConnectivity(int[] list)
        {
            int count = list[6] - list[6] * list[7] * list[0];
            count += list[0] - list[0] * list[1] * list[2];
            count += list[2] - list[2] * list[3] * list[4];
            count += list[4] - list[4] * list[5] * list[6];
            return count;
        }

        public Image Sceletonization(Image image)
        {
            int[,] data = ImageDataConverter.ToInteger32((Bitmap)image);

             int[,] newdata = ThinnerHilditch(data);

             return ImageDataConverter.ToBitmap32(newdata);  

          //  return hilditch((Bitmap)image);  
        }
        int func_nc8(int[] b)
        /* connectivity detection for each point */
        {
            int[] n_odd = { 1, 3, 5, 7 };  /* odd-number neighbors */
            int i, j, sum;           /* control variable */
            int[] d = new int[10];
            for (i = 0; i <= 9; i++)
            {
                j = i;
                if (i == 9) j = 1;
                if (Math.Abs(b[j]) == 1)
                {
                    d[i] = 1;
                }
                else {
                    d[i] = 0;
                }
            }
            sum = 0;
            for (i = 0; i < 4; i++)
            {
                j = n_odd[i];
                sum = sum + d[j] - d[j] * d[j + 1] * d[j + 2];
            }
            return (sum);
        }



        public Bitmap hilditch(Bitmap bmp1)
        /* thinning of binary image by Hilditch's algorithm */
        /* WHITE --> 0, GRAY --> -1, BLACK --> 1 */
        /* input image1[y][x] ===> output image2[y][x] */
        {
            int[,] offset = { { 0, 0 }, { 1, 0 }, { 1, -1 }, { 0, -1 }, { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, 1 }, { 1, 1 } }; /* offsets for neighbors */

            int[] n_odd = { 1, 3, 5, 7 };      /* odd-number neighbors */
            int px, py;                         /* X/Y coordinates  */
            int[] b = new int[9];                           /* gray levels for 9 neighbors */
            int[] condition = new int[6];                   /* valid for conditions 1-6 */
            int counter;                        /* number of changing points  */
            int i, x, y, copy, sum;             /* control variable          */

            var y_size1 = bmp1.Height;
            var x_size1 = bmp1.Width;

            var output = ImageDataConverter.ToInteger32(bmp1);

            /* processing starts */
            do
            {
                counter = 0;
            for (y = 0; y < y_size1; y++)
            {
                for (x = 0; x < x_size1; x++)
                {
                    /* substitution of 9-neighbor gray values */
                    for (i = 0; i < 9; i++)
                    {
                        b[i] = 0;



                        px = x + offset[i, 0];
                        py = y + offset[i, 1];
                        if (px >= 0 && px < x_size1 &&

                            py >= 0 && py < y_size1)
                        {
                            if (output[py, px] == 0)
                            {
                                b[i] = 1;
                            }
                            else if (output[py, px] == 128)
                            {
                                b[i] = -1;
                            }
                        }
                    }
                    for (i = 0; i < 6; i++)
                    {
                        condition[i] = 0;
                    }

                    /* condition 1: figure point */
                    if (b[0] == 1) condition[0] = 1;

                    /* condition 2: boundary point */
                    sum = 0;
                    for (i = 0; i < 4; i++)
                    {
                        sum = sum + 1 - Math.Abs(b[n_odd[i]]);
                    }
                    if (sum >= 1) condition[1] = 1;

                    /* condition 3: endpoint conservation */
                    sum = 0;
                    for (i = 1; i <= 8; i++)
                    {
                        sum = sum + Math.Abs(b[i]);
                    }
                    if (sum >= 2) condition[2] = 1;

                    /* condition 4: isolated point conservation */
                    sum = 0;
                    for (i = 1; i <= 8; i++)
                    {
                        if (b[i] == 1) sum++;
                    }
                    if (sum >= 1) condition[3] = 1;

                    /* condition 5: connectivity conservation */
                    if (func_nc8(b) == 1) condition[4] = 1;

                    /* condition 6: one-side elimination for line-width of two */
                    sum = 0;
                    for (i = 1; i <= 8; i++)
                    {
                        if (b[i] != -1)
                        {
                            sum++;
                        }
                        else {
                            copy = b[i];
                            b[i] = 0;
                            if (func_nc8(b) == 1) sum++;
                            b[i] = copy;
                        }
                    }
                    if (sum == 8) condition[5] = 1;

                    /* final decision */
                    if (condition[0] == 1 && condition[1] == 1 && condition[2] == 1 &&
                        condition[3] == 1 && condition[4] == 1 && condition[5] == 1)
                    {
                        Console.WriteLine("Counter final {0}", counter);
                        output[y, x] = 128; /* equals -1 */
                        counter++;
                    }
                } /* end of x */
            } /* end of y */

            if (counter != 0)
            {
                for (y = 0; y < y_size1; y++)
                {
                    for (x = 0; x < x_size1; x++)
                    {
                        if (output[y, x] == 128) output[y, x] = 0;
                    }
                }
            }
            Console.WriteLine(counter);
        } while (counter != 0);

            return ImageDataConverter.ToBitmap32(output);
        }
    }
}
