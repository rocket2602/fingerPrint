﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Sceletonization
{
    public class ZhangSuenThinning : ISceletonization
    {
        public  Image Sceletonization(Image image)
        {
            bool[][] image2dArray = Image2Bool(image);

            bool[][] temp = ArrayClone(image2dArray);

            int count = 0;
            do  // the missing iteration
            {
                count = step(1, temp, image2dArray);
                temp = ArrayClone(image2dArray);      // ..and on each..
                count += step(2, temp, image2dArray);
                temp = ArrayClone(image2dArray);      // ..call!
            }
            while (count > 0);

            return Bool2Image(temp);
        }

        private T[][] ArrayClone<T>(T[][] A) => A.Select(a => a.ToArray()).ToArray();

        private int step(int stepNo, bool[][] temp, bool[][] s)
        {
            int count = 0;

            for (int a = 1; a < temp.Length - 1; a++)
            {
                for (int b = 1; b < temp[0].Length - 1; b++)
                {
                    if (SuenThinningAlg(a, b, temp, stepNo == 2))
                    {
                        // still changes happening?
                        if (s[a][b]) count++;
                        s[a][b] = false;
                    }
                }
            }
            return count;
        }

        private bool SuenThinningAlg(int x, int y, bool[][] s, bool even)
        {
            bool p2 = s[x][y - 1];
            bool p3 = s[x + 1][y - 1];
            bool p4 = s[x + 1][y];
            bool p5 = s[x + 1][y + 1];
            bool p6 = s[x][y + 1];
            bool p7 = s[x - 1][y + 1];
            bool p8 = s[x - 1][y];
            bool p9 = s[x - 1][y - 1];


            int bp1 = NumberOfNonZeroNeighbors(x, y, s);
            if (bp1 >= 2 && bp1 <= 6) //2nd condition
            {
                if (NumberOfZeroToOneTransitionFromP9(x, y, s) == 1)
                {
                    if (even)
                    {
                        if (!((p2 && p4) && p8))
                        {
                            if (!((p2 && p6) && p8))
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        if (!((p2 && p4) && p6))
                        {
                            if (!((p4 && p6) && p8))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private int NumberOfZeroToOneTransitionFromP9(int x, int y, bool[][] s)
        {
            bool p2 = s[x][y - 1];
            bool p3 = s[x + 1][y - 1];
            bool p4 = s[x + 1][y];
            bool p5 = s[x + 1][y + 1];
            bool p6 = s[x][y + 1];
            bool p7 = s[x - 1][y + 1];
            bool p8 = s[x - 1][y];
            bool p9 = s[x - 1][y - 1];

            int A = Convert.ToInt32((!p2 && p3)) + Convert.ToInt32((!p3 && p4)) +
                    Convert.ToInt32((!p4 && p5)) + Convert.ToInt32((!p5 && p6)) +
                    Convert.ToInt32((!p6 && p7)) + Convert.ToInt32((!p7 && p8)) +
                    Convert.ToInt32((!p8 && p9)) + Convert.ToInt32((!p9 && p2));
            return A;
        }
        private int NumberOfNonZeroNeighbors(int x, int y, bool[][] s)
        {
            int count = 0;
            if (s[x - 1][y]) count++;
            if (s[x - 1][y + 1]) count++;
            if (s[x - 1][y - 1]) count++;
            if (s[x][y + 1]) count++;
            if (s[x][y - 1]) count++;
            if (s[x + 1][y]) count++;
            if (s[x + 1][y + 1]) count++;
            if (s[x + 1][y - 1]) count++;
            return count;
        }


        private bool[][] Image2Bool(Image img)
        {
            Bitmap bmp = new Bitmap(img);
            bool[][] s = new bool[bmp.Height][];
            for (int y = 0; y < bmp.Height; y++)
            {
                s[y] = new bool[bmp.Width];
                for (int x = 0; x < bmp.Width; x++)
                    s[y][x] = bmp.GetPixel(x, y).GetBrightness() < 0.3;
            }
            return s;

        }

        private Image Bool2Image(bool[][] s)
        {
            Bitmap bmp = new Bitmap(s[0].Length, s.Length);
            using (Graphics g = Graphics.FromImage(bmp)) g.Clear(Color.White);
            for (int y = 0; y < bmp.Height; y++)
                for (int x = 0; x < bmp.Width; x++)
                    if (s[y][x]) bmp.SetPixel(x, y, Color.Black);

            return (Bitmap)bmp;
        }

        //public static void ImageToGrayscale(Bitmap bmp)
        //{
        //    int width = bmp.Width;
        //    int height = bmp.Height;

        //    Color p;

        //    //grayscale
        //    for (int y = 0; y < height; y++)
        //    {
        //        for (int x = 0; x < width; x++)
        //        {
        //            //get pixel value
        //            p = bmp.GetPixel(x, y);

        //            //extract pixel component ARGB
        //            int a = p.A;
        //            int r = p.R;
        //            int g = p.G;
        //            int b = p.B;

        //            //find average
        //            int avg = (r + g + b) / 3;

        //            //set new pixel value
        //            bmp.SetPixel(x, y, Color.FromArgb(a, avg, avg, avg));
        //        }
        //    }
        //}

        //public static void SaveAsBitmap(string fileName, int width, int height, byte[] imageData)
        //{
        //    // Need to copy our 8 bit greyscale image into a 32bit layout.
        //    // Choosing 32bit rather than 24 bit as its easier to calculate stride etc.
        //    // This will be slow enough and isn't the most efficient method.
        //    var data = new byte[width * height * 4];

        //    int o = 0;

        //    for (var i = 0; i < width * height; i++)
        //    {
        //        var value = imageData[i];

        //        // Greyscale image so r, g, b, get the same
        //        // intensity value.
        //        data[o++] = value;
        //        data[o++] = value;
        //        data[o++] = value;
        //        data[o++] = 0;  // Alpha isn't actually used
        //    }

        //    unsafe
        //    {
        //        fixed (byte* ptr = data)
        //        {
        //            // Craete a bitmap wit a raw pointer to the data
        //            using (Bitmap image = new Bitmap(width, height, width * 4,PixelFormat.Format32bppRgb, new IntPtr(ptr)))
        //            {
        //                // And save it.
        //                image.Save(Path.ChangeExtension(fileName, ".bmp"));
        //            }
        //        }
        //    }
        //}

    }
}
