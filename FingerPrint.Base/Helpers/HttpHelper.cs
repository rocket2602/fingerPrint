﻿using FingerPrint.Base.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FingerPrint.Base.Helpers
{
    public static class HttpHelper
    {

        private readonly static string _baseUrl;


        static HttpHelper()
        {
            _baseUrl = ConfigurationManager.AppSettings["apiUrl"];
        }

        public static async Task<HttpResponseMessage> PostBson<T>(T data)
        {
            using (var client = new HttpClient() { BaseAddress = new Uri(_baseUrl) })
            {
                //Specifiy 'Accept' header As BSON: to ask server to return data as BSON format
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/bson"));

                //Specify 'Content-Type' header: to tell server which format of the data will be posted
                //Post data will be as Bson format                
                var bSonData = SerializeBson<T>(data);
                var byteArrayContent = new ByteArrayContent(bSonData);
                byteArrayContent.Headers.ContentType = new MediaTypeHeaderValue("application/bson");

                var response = await client.PostAsync("finger/template/save", byteArrayContent);

                response.EnsureSuccessStatusCode();

                return response;
            }
        }

        public static byte[] SerializeBson<T>(T obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (BsonWriter writer = new BsonWriter(ms))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(writer, obj);
                }

                return ms.ToArray();
            }
        }




        public static async Task<FingerViewModel> Identify(byte[] template,int row, int column,int minutia)
        {
            using (var client = new HttpClient() { BaseAddress = new Uri(_baseUrl) })
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Specify 'Content-Type' header: to tell server which format of the data will be posted
                //Post data will be as Bson format   
                var obj = new { Content = template,Row  = row, Column = column, Minutia = minutia };

                var bSonData = SerializeBson<object>(obj);


                var byteArrayContent = new ByteArrayContent(bSonData);
                byteArrayContent.Headers.ContentType = new MediaTypeHeaderValue("application/bson");

                var response = await client.PostAsync("finger/template/identify", byteArrayContent);

                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<FingerViewModel>(new[] { new JsonMediaTypeFormatter() });
                }

                throw new Exception("Huston, we have a problem");

            }
        }


        public static async Task<FingerViewModel> Register(byte[] template)
        {
            using (var client = new HttpClient() { BaseAddress = new Uri(_baseUrl) })
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Specify 'Content-Type' header: to tell server which format of the data will be posted
                //Post data will be as Bson format   
                var obj = new { Content = template };

                var bSonData = SerializeBson<object>(obj);


                var byteArrayContent = new ByteArrayContent(bSonData);
                byteArrayContent.Headers.ContentType = new MediaTypeHeaderValue("application/bson");

                var response = await client.PostAsync("finger/template/set", byteArrayContent);

                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<FingerViewModel>(new[] { new JsonMediaTypeFormatter() });
                }

                throw new Exception("Huston, we have a problem");

            }
        }
    }
}
